"""
Tests for the workouts application.
"""
from django.test import TestCase, RequestFactory, Client
from workouts.models import Workout, Exercise, ExerciseInstance
from users.models import User
import json
# Create your tests here.
    
# class WorkoutTest(TestCase):
#   def setUp(self):
#     self.client = Client()
#     self.user1 = User.objects.create(username="user1")
#     self.user1.set_password('12345')
#     self.user1.save()
#     response = self.client.post('/api/token/', {'username': 'user1', 'password': '12345'})
#     content = json.loads(response.content)
#     self.client.defaults['HTTP_AUTHORIZATION'] = 'Bearer ' + content['access']
  
#   def test_workout_normal(self):
#     exercise = Exercise.objects.create(name="Pull-ups", description="Pull-ups", unit="Times")
#     data = {
#       "name": "Workout",
#       "date": "2021-03-05T12:00:00",
#       "notes": "Notes",
#       "visibility": "PU",
#       "exercise_instances": [{"exercise":"http://localhost:8000/api/exercises/1/","number":12,"sets":12}],
#       "files": [],
#     }

#     response = self.client.post('/api/workouts/', data, follow=True)
#     print(response.content)
#     self.assertEqual(response.status_code, 201)