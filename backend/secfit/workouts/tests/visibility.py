from django.test import TestCase, RequestFactory, Client
from workouts.models import Workout, Exercise, ExerciseInstance, WorkoutFile
from users.models import User
from comments.models import Comment

import json

class VisibilityTest(TestCase):
  def setUp(self):
    self.client = Client()
    self.coach = User.objects.create(username="coach")
    self.coach.set_password('12345')
    self.coach.save()
    self.athlete = User.objects.create(username="athlete", coach=self.coach)
    self.athlete.set_password('12345')
    self.athlete.save()
    self.other = User.objects.create(username="other")


  def test_athlete_sees_workouts(self):
    response = self.client.post('/api/token/', {'username': 'athlete', 'password': '12345'})
    content = json.loads(response.content)
    self.client.defaults['HTTP_AUTHORIZATION'] = 'Bearer ' + content['access']

    # Athlete can see owned private workout
    workout = Workout.objects.create(name="Workout", date="2021-03-05T12:00:00Z", notes="Notes", owner=self.athlete, visibility="PR")
    response = self.client.get('/api/workouts/'+str(workout.id)+'/')
    self.assertEqual(response.status_code, 200)
    comment = Comment.objects.create(content="COMMENT", timestamp="2021-03-05T12:00:00Z", owner=self.athlete, workout=workout)
    response = self.client.get('/api/comments/'+str(comment.id)+'/')
    self.assertEqual(response.status_code, 200)
    file = WorkoutFile.objects.create(file='some/path/to/file', owner=self.athlete, workout=workout)
    response = self.client.get('/api/workout-files/'+str(file.id)+'/')
    self.assertEqual(response.status_code, 200)

    # Athlete can see public workout
    workout = Workout.objects.create(name="Workout", date="2021-03-05T12:00:00Z", notes="Notes", owner=self.other, visibility="PU")
    response = self.client.get('/api/workouts/'+str(workout.id)+'/')
    self.assertEqual(response.status_code, 200)
    comment = Comment.objects.create(content="COMMENT", timestamp="2021-03-05T12:00:00Z", owner=self.other, workout=workout)
    response = self.client.get('/api/comments/'+str(comment.id)+'/')
    self.assertEqual(response.status_code, 200)
    file = WorkoutFile.objects.create(file='some/path/to/file', owner=self.other, workout=workout)
    response = self.client.get('/api/workout-files/'+str(file.id)+'/')
    self.assertEqual(response.status_code, 200)

    # Athlete cannot see private workout
    workout = Workout.objects.create(name="Workout", date="2021-03-05T12:00:00Z", notes="Notes", owner=self.other, visibility="PR")
    response = self.client.get('/api/workouts/'+str(workout.id)+'/')
    self.assertEqual(response.status_code, 403)
    comment = Comment.objects.create(content="COMMENT", timestamp="2021-03-05T12:00:00Z", owner=self.other, workout=workout)
    response = self.client.get('/api/comments/'+str(comment.id)+'/')
    self.assertEqual(response.status_code, 403)
    file = WorkoutFile.objects.create(file='some/path/to/file', owner=self.other, workout=workout)
    response = self.client.get('/api/workout-files/'+str(file.id)+'/')
    self.assertEqual(response.status_code, 403)

    # Athlete cannot see coach-visibility workout
    workout = Workout.objects.create(name="Workout", date="2021-03-05T12:00:00Z", notes="Notes", owner=self.other, visibility="CO")
    response = self.client.get('/api/workouts/'+str(workout.id)+'/')
    self.assertEqual(response.status_code, 403)
    comment = Comment.objects.create(content="COMMENT", timestamp="2021-03-05T12:00:00Z", owner=self.other, workout=workout)
    response = self.client.get('/api/comments/'+str(comment.id)+'/')
    self.assertEqual(response.status_code, 403)
    file = WorkoutFile.objects.create(file='some/path/to/file', owner=self.other, workout=workout)
    response = self.client.get('/api/workout-files/'+str(file.id)+'/')
    self.assertEqual(response.status_code, 403)

  def test_coach_sees_workouts(self):
    response = self.client.post('/api/token/', {'username': 'coach', 'password': '12345'})
    content = json.loads(response.content)
    self.client.defaults['HTTP_AUTHORIZATION'] = 'Bearer ' + content['access']

    # Coach can see public athlete workout
    workout = Workout.objects.create(name="Workout", date="2021-03-05T12:00:00Z", notes="Notes", owner=self.athlete, visibility="PU")
    response = self.client.get('/api/workouts/'+str(workout.id)+'/')
    self.assertEqual(response.status_code, 200)
    comment = Comment.objects.create(content="COMMENT", timestamp="2021-03-05T12:00:00Z", owner=self.athlete, workout=workout)
    response = self.client.get('/api/comments/'+str(comment.id)+'/')
    self.assertEqual(response.status_code, 200)
    file = WorkoutFile.objects.create(file='some/path/to/file', owner=self.athlete, workout=workout)
    response = self.client.get('/api/workout-files/'+str(file.id)+'/')
    self.assertEqual(response.status_code, 200)

    # Coach can see coach-visibility athlete workout
    workout = Workout.objects.create(name="Workout", date="2021-03-05T12:00:00Z", notes="Notes", owner=self.athlete, visibility="CO")
    response = self.client.get('/api/workouts/'+str(workout.id)+'/')
    self.assertEqual(response.status_code, 200)
    comment = Comment.objects.create(content="COMMENT", timestamp="2021-03-05T12:00:00Z", owner=self.coach, workout=workout)
    response = self.client.get('/api/comments/'+str(comment.id)+'/')
    self.assertEqual(response.status_code, 200)
    file = WorkoutFile.objects.create(file='some/path/to/file', owner=self.coach, workout=workout)
    response = self.client.get('/api/workout-files/'+str(file.id)+'/')
    self.assertEqual(response.status_code, 200)

    # Coach can see public other workout
    workout = Workout.objects.create(name="Workout", date="2021-03-05T12:00:00Z", notes="Notes", owner=self.other, visibility="PU")
    response = self.client.get('/api/workouts/'+str(workout.id)+'/')
    self.assertEqual(response.status_code, 200)
    comment = Comment.objects.create(content="COMMENT", timestamp="2021-03-05T12:00:00Z", owner=self.coach, workout=workout)
    response = self.client.get('/api/comments/'+str(comment.id)+'/')
    self.assertEqual(response.status_code, 200)
    file = WorkoutFile.objects.create(file='some/path/to/file', owner=self.coach, workout=workout)
    response = self.client.get('/api/workout-files/'+str(file.id)+'/')
    self.assertEqual(response.status_code, 200)

    # Coach cannot see private athlete workout
    workout = Workout.objects.create(name="Workout", date="2021-03-05T12:00:00Z", notes="Notes", owner=self.athlete, visibility="PR")
    response = self.client.get('/api/workouts/'+str(workout.id)+'/')
    self.assertEqual(response.status_code, 403)
    comment = Comment.objects.create(content="COMMENT", timestamp="2021-03-05T12:00:00Z", owner=self.other, workout=workout)
    response = self.client.get('/api/comments/'+str(comment.id)+'/')
    self.assertEqual(response.status_code, 403)
    file = WorkoutFile.objects.create(file='some/path/to/file', owner=self.athlete, workout=workout)
    response = self.client.get('/api/workout-files/'+str(file.id)+'/')
    self.assertEqual(response.status_code, 403)

    # Coach cannot see private other workout
    workout = Workout.objects.create(name="Workout", date="2021-03-05T12:00:00Z", notes="Notes", owner=self.other, visibility="PR")
    response = self.client.get('/api/workouts/'+str(workout.id)+'/')
    self.assertEqual(response.status_code, 403)
    comment = Comment.objects.create(content="COMMENT", timestamp="2021-03-05T12:00:00Z", owner=self.other, workout=workout)
    response = self.client.get('/api/comments/'+str(comment.id)+'/')
    self.assertEqual(response.status_code, 403)
    file = WorkoutFile.objects.create(file='some/path/to/file', owner=self.other, workout=workout)
    response = self.client.get('/api/workout-files/'+str(file.id)+'/')
    self.assertEqual(response.status_code, 403)
    

    
    # Coach cannot see coach-visibility other workout
    workout = Workout.objects.create(name="Workout", date="2021-03-05T12:00:00Z", notes="Notes", owner=self.other, visibility="CO")
    response = self.client.get('/api/workouts/'+str(workout.id)+'/')
    self.assertEqual(response.status_code, 403)
    comment = Comment.objects.create(content="COMMENT", timestamp="2021-03-05T12:00:00Z", owner=self.other, workout=workout)
    response = self.client.get('/api/comments/'+str(comment.id)+'/')
    self.assertEqual(response.status_code, 403)
    file = WorkoutFile.objects.create(file='some/path/to/file', owner=self.other, workout=workout)
    response = self.client.get('/api/workout-files/'+str(file.id)+'/')
    self.assertEqual(response.status_code, 403)