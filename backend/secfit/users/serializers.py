from rest_framework import serializers
from django.contrib.auth import get_user_model, password_validation
from users.models import Offer, AthleteFile, AthleteGroup
from django import forms


class UserSerializer(serializers.HyperlinkedModelSerializer):
    password = serializers.CharField(style={"input_type": "password"}, write_only=True)
    password1 = serializers.CharField(style={"input_type": "password"}, write_only=True)

    class Meta:
        model = get_user_model()
        fields = [
            "url",
            "id",
            "email",
            "username",
            "password",
            "password1",
            "athletes",
            "phone_number",
            "country",
            "city",
            "street_address",
            "coach",
            "workouts",
            "coach_files",
            "athlete_files",
            "visibility",
        ]

    def validate_password(self, value):
        data = self.get_initial()

        password = data.get("password")
        password1 = data.get("password1")

        try:
            password_validation.validate_password(password)
        except forms.ValidationError as error:
            raise serializers.ValidationError(error.messages)

        return value

    def create(self, validated_data):
        username = validated_data["username"]
        email = validated_data["email"]
        password = validated_data["password"]
        phone_number = validated_data["phone_number"]
        country = validated_data["country"]
        city = validated_data["city"]
        street_address = validated_data["street_address"]
        user_obj = get_user_model()(username=username, email=email, phone_number=phone_number, country=country, city=city, street_address=street_address)
        user_obj.set_password(password)
        user_obj.save()

        return user_obj


class UserListSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = get_user_model()
        fields = [
            "url",
            "id",
            "username",
            "visibility",
        ]

class UserGetSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = get_user_model()
        fields = [
            "url",
            "id",
            "email",
            "username",
            "athletes",
            "phone_number",
            "country",
            "city",
            "street_address",
            "coach",
            "workouts",
            "coach_files",
            "athlete_files",
            "athlete_group_id",
            "visibility",
        ]

class VisibilityAwareUserSerializer(serializers.HyperlinkedModelSerializer):

    class Meta:
        model = get_user_model()
        fields = [
            "url",
            "id",
            "username",
            "visibility",
            "email",
            "athletes",
            "phone_number",
            "country",
            "city",
            "street_address",
            "coach",
            "workouts",
            "coach_files",
            "athlete_files",
            "athlete_group_id",
        ]

    def to_representation(self, instance):
        ret = super(VisibilityAwareUserSerializer, self).to_representation(instance)
        [ret.pop(field,'') for field in self.fieldsToPop(self.context['request'].user, instance)]
        return ret

    def fieldsToPop(self, user, obj): 
        if (obj.visibility == "PU") or (obj.visibility == "CO" and (obj.coach == user or obj == user)) or (obj.visibility == "PR" and obj == user): 
            return []
        elif obj.visibility == "PR" and obj.coach == user:
            return [
                "email",
                "athletes",
                "phone_number",
                "country",
                "city",
                "street_address",
                "coach_files",
            ]
        else:
            return [
                "coach",
                "athlete_files",
                "athlete_group_id",
                "email",
                "athletes",
                "phone_number",
                "country",
                "city",
                "street_address",
                "workouts",
                "coach_files",
            ]


class UserPutSerializer(serializers.ModelSerializer):
    class Meta:
        model = get_user_model()
        fields = [
            "email",
            "username",
            "phone_number",
            "country",
            "city",
            "street_address",
            "visibility",
        ]

class UserAthletesPutSerializer(serializers.ModelSerializer):
    class Meta:
        model = get_user_model()
        fields = ["athletes"]

    def update(self, instance, validated_data):
        athletes_data = validated_data["athletes"]
        instance.athletes.set(athletes_data)

        return instance


class AthleteFileSerializer(serializers.HyperlinkedModelSerializer):
    owner = serializers.ReadOnlyField(source="owner.username")

    class Meta:
        model = AthleteFile
        fields = ["url", "id", "owner", "file", "athlete"]

    def create(self, validated_data):
        return AthleteFile.objects.create(**validated_data)


class OfferSerializer(serializers.HyperlinkedModelSerializer):
    owner = serializers.ReadOnlyField(source="owner.username")

    class Meta:
        model = Offer
        fields = [
            "url",
            "id",
            "owner",
            "recipient",
            "status",
            "timestamp",
        ]


class AthleteGroupSerializer(serializers.ModelSerializer):

    class Meta:
        model = AthleteGroup
        fields = [
            "id",
            "title",
            "description",
            "owner_id",
        ]

    def create(self, validated_data):
        validated_data['owner_id'] = self.context['request'].user.id
        return AthleteGroup.objects.create(**validated_data)
        

class GetAthleteGroupSerializer(serializers.ModelSerializer):
    athletes = serializers.SerializerMethodField()

    class Meta:
        model = AthleteGroup
        fields = [
            "id",
            "title",
            "description",
            "owner_id",
            "athletes",
        ]

    def get_athletes(self, obj):
        return UserListSerializer(get_user_model().objects.all().filter(athlete_group_id=obj.id), context=self.context, many=True).data

    def retrieve(self, validated_data):
        validated_data['owner_id'] = self.context['request'].user.id
        return AthleteGroup.objects.create(**validated_data)

    
class UserAthleteGroupSerializer(serializers.ModelSerializer):
    class Meta:
        model = get_user_model()
        fields = [ "athlete_group_id" ]

    def update(self, instance, validated_data):
        setattr(instance, "athlete_group_id", self.context['request'].data.get("athlete_group_id", None))
        instance.save()
        return instance
    
