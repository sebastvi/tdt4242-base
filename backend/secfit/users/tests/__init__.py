from users.tests.permissions import *
from users.tests.user_registration import *
from users.tests.user_serializer import *
from users.tests.athlete_group import *
from users.tests.update_user import *