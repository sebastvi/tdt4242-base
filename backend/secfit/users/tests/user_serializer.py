from django.test import TestCase, RequestFactory, Client
from users.serializers import UserSerializer
from users.models import User, AthleteFile


# Create your tests here.

class UserTest(TestCase):
  def setUp(self):
    self.user1 = User.objects.create(username="user1")
    self.user2 = User.objects.create(username="user2", coach=self.user1)
    self.factory = RequestFactory()
  
  def test_user_validate_password(self):
    serializer = UserSerializer(data = {'password': 'password','password1': 'password'})

    password_validation = serializer.validate_password("password") 
    self.assertEqual(password_validation, "password")

  def test_user_create(self):
    values = {  
      'email': 'a@aa.aa',
      'username': 'athlete',
      'password': 'password',
      'phone_number': 123456789,
      'country': 'Norway',
      'city': 'Trondheim',
      'street_address': 'Gløs',
    }
    serializer = UserSerializer(data=values)

    user = serializer.create(values) 
    self.assertEqual(user.username, values['username'])
    self.assertEqual(user.email, values['email'])
    self.assertEqual(user.phone_number, values['phone_number'])
    self.assertEqual(user.country, values['country'])
    self.assertEqual(user.city, values['city'])
    self.assertEqual(user.street_address, values['street_address'])
    # self.assertEqual(user.password, 'password')



