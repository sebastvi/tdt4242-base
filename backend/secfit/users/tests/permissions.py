from django.test import TestCase, RequestFactory, Client
from users.permissions import IsCurrentUser, IsAthlete, IsCoach
from users.models import User, AthleteFile


# Create your tests here.

class IsCurrentUserTest(TestCase):
  def setUp(self):
    self.user1 = User.objects.create(username="user1")
    self.user2 = User.objects.create(username="user2", coach=self.user1)
    self.factory = RequestFactory()
    self.is_current_user_check = IsCurrentUser()

  def test_current_user_true(self):
    request = self.factory.get('/')
    request.user = self.user1

    permission = self.is_current_user_check.has_object_permission(request, None, self.user1)

    self.assertTrue(permission)

  def test_current_user_false(self):
    request = self.factory.get('/')
    request.user = self.user1

    permission = self.is_current_user_check.has_object_permission(request, None, self.user2)

    self.assertFalse(permission)
  
class IsAthleteTest(TestCase):
  def setUp(self):
    self.user1 = User.objects.create(username="user1")
    self.user2 = User.objects.create(username="user2", coach=self.user1)
    self.factory = RequestFactory()
    self.is_athlete_check = IsAthlete()

  def test_athlete_get(self):
    request = self.factory.get('/')
    request.user = self.user2

    permission = self.is_athlete_check.has_permission(request, None)

    self.assertTrue(permission)

  def test_athlete_post(self):
    request = self.factory.post('/')
    request.user = self.user2
    request.data = {'athlete' : 'http://localhost:8000/api/users/2/'}

    permission = self.is_athlete_check.has_permission(request, None)

    self.assertTrue(permission)

  def test_athlete_object(self):
    request = self.factory.get('/')
    request.user = self.user2

    file = AthleteFile.objects.create(athlete=self.user2, owner=self.user1)
    permission = self.is_athlete_check.has_object_permission(request, None, file)

    self.assertTrue(permission)

class IsCoachTest(TestCase):
  def setUp(self):
    self.user1 = User.objects.create(username="user1")
    self.user2 = User.objects.create(username="user2", coach=self.user1)
    self.factory = RequestFactory()
    self.is_coach_check = IsCoach()

  def test_coach_get(self):
    request = self.factory.get('/')
    request.user = self.user1

    permission = self.is_coach_check.has_permission(request, None)

    self.assertTrue(permission)

  def test_coach_post(self):
    request = self.factory.post('/')
    request.user = self.user1
    request.data = {'athlete' : 'http://localhost:8000/api/users/2/'}

    permission = self.is_coach_check.has_permission(request, None)

    self.assertTrue(permission)

  def test_coach_object(self):
    request = self.factory.get('/')
    request.user = self.user1

    file = AthleteFile.objects.create(athlete=self.user2, owner=self.user1)
    permission = self.is_coach_check.has_object_permission(request, None, file)

    self.assertTrue(permission)