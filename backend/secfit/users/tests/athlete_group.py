from django.test import TestCase, RequestFactory, Client
from users.models import User, AthleteGroup
import json


class AthleteGroupTest(TestCase):
  def setUp(self):
    self.client = Client()
    self.user1 = User.objects.create(username="user1")
    self.user2 = User.objects.create(username="user2", coach=self.user1)
    self.user1.set_password('12345')
    self.user1.save()
    response = self.client.post('/api/token/', {'username': 'user1', 'password': '12345'})
    content = json.loads(response.content)
    self.client.defaults['HTTP_AUTHORIZATION'] = 'Bearer ' + content['access']
    self.athleteGroup = AthleteGroup.objects.create(title='Håndball', description="De som kaster ball", owner_id=1)

  def test_athlete_group_post(self):
    data = {
      "title": "Fotball",
      "description": "De som sparker ball",
    }
    response = self.client.post('/api/athlete-groups/', data)
    self.assertEqual(response.status_code, 201)

  def test_athlete_group_get(self):
    response = self.client.get('/api/athlete-groups/1/')
    self.assertEqual(response.status_code, 200)

  def test_athlete_group_put(self):
    data = {
      "title": "Basketball",
      "description": "De som kaster ball i korg",
    }
    response = self.client.put('/api/athlete-groups/1/', data, content_type="application/json")
    self.assertEqual(response.status_code, 200)

  def test_add_athlete_to_athlete_group_patch(self):
    data = {
      "athlete_group_id": "1",
    }
    response = self.client.patch('/api/users/'+str(self.user2.id)+'/athlete-groups', data, content_type="application/json")
    self.assertEqual(response.status_code, 200)
    
    # Check that the user is a member of the athlete_group
    response1 = self.client.get('/api/athlete-groups/1/')
    content = json.loads(response1.content)
    self.assertEqual(content['athletes'][0]['id'], self.user2.id)



  def test_athlete_group_delete(self):
    response = self.client.delete('/api/athlete-groups/1/')
    self.assertEqual(response.status_code, 204)