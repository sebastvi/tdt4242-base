from django.test import TestCase, RequestFactory, Client
from users.models import User, AthleteGroup
import json


class UserUpdateTest(TestCase):
  def setUp(self):
    self.client = Client()
    self.user1 = User.objects.create(username="user1")
    self.user1.set_password('12345')
    self.user1.save()
    response = self.client.post('/api/token/', {'username': 'user1', 'password': '12345'})
    content = json.loads(response.content)
    self.client.defaults['HTTP_AUTHORIZATION'] = 'Bearer ' + content['access']

  def test_update_user(self):
    data = {
      'email': 'a@aa.aa',
      'username': 'Athlete1234',
      'password': '#¤252!"#AWda',
      'password1': '#¤252!"#AWda',
      'phone_number': 123456789,
      'country': 'Norway',
      'city': 'Trondheim',
      'street_address': 'Gløs',
    }
    response = self.client.put('/api/users/1/', data, content_type='application/json')
    self.assertEqual(response.status_code, 200)
