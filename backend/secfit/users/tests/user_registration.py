from django.test import TestCase, RequestFactory, Client
from users.models import User, AthleteFile


# Create your tests here.
class UserRegistrationTest(TestCase):
  def setUp(self):
    self.client = Client()
  
  def test_user_registration_mormal(self):
    data = {
      'email': 'a@aa.aa',
      'username': 'Athlete1234',
      'password': '#¤252!"#AWda',
      'password1': '#¤252!"#AWda',
      'phone_number': 123456789,
      'country': 'Norway',
      'city': 'Trondheim',
      'street_address': 'Gløs',
    }

    response = self.client.post('/api/users/', data)
    self.assertEqual(response.status_code, 201)
  
  def test_user_registration_only_required_info(self):
    data = {
      'email': '',
      'username': 'Athlete1234',
      'password': '#¤252!"#AWda',
      'password1': '#¤252!"#AWda',
      'phone_number': '',
      'country': '',
      'city': '',
      'street_address': '',
    }

    response = self.client.post('/api/users/', data)
    self.assertEqual(response.status_code, 201)

  def test_user_registration_no_password(self):
    data = {
      'email': 'a@aa.aa',
      'username': 'Athlete1234',
      'password': '',
      'password1': '',
      'phone_number': 123456789,
      'country': 'Norway',
      'city': 'Trondheim',
      'street_address': 'Gløs',
    }

    response = self.client.post('/api/users/', data)
    self.assertEqual(response.status_code, 400)


  def test_user_registration_no_username(self):
    data = {
      'email': 'a@aa.aa',
      'username': '',
      'password': '#¤252!"#AWda',
      'password1': '#¤252!"#AWda',
      'phone_number': 123456789,
      'country': 'Norway',
      'city': 'Trondheim',
      'street_address': 'Gløs',
    }

    response = self.client.post('/api/users/', data)
    self.assertEqual(response.status_code, 400)


  def test_user_registration_no_password1(self):
    data = {
      'email': 'a@aa.aa',
      'username': 'Athlete1234',
      'password': '#¤252!"#AWda',
      'password1': '',
      'phone_number': 123456789,
      'country': 'Norway',
      'city': 'Trondheim',
      'street_address': 'Gløs',
    }

    response = self.client.post('/api/users/', data)
    self.assertEqual(response.status_code, 400)

  def test_user_registration_bad_email(self):
    data = {
      'email': 'a@aa',
      'username': 'Athlete1234',
      'password': '#¤252!"#AWda',
      'password1': '#¤252!"#AWda',
      'phone_number': 123456789,
      'country': 'Norway',
      'city': 'Trondheim',
      'street_address': 'Gløs',
    }

    response = self.client.post('/api/users/', data)
    self.assertEqual(response.status_code, 400)

  def test_user_registration_username_allowed_special_characters(self):
    data = {
      'email': 'a@aa.aa',
      'username': '@.+-_Athlete1234',
      'password': '#¤252!"#AWda',
      'password1': '#¤252!"#AWda',
      'phone_number': 123456789,
      'country': 'Norway',
      'city': 'Trondheim',
      'street_address': 'Gløs',
    }

    response = self.client.post('/api/users/', data)
    self.assertEqual(response.status_code, 201)

  def test_user_registration_username_not_allowed_special_characters(self):
    data = {
      'email': 'a@aa.aa',
      'username': '[?!Athlete1234',
      'password': '#¤252!"#AWda',
      'password1': '#¤252!"#AWda',
      'phone_number': 123456789,
      'country': 'Norway',
      'city': 'Trondheim',
      'street_address': 'Gløs',
    }

    response = self.client.post('/api/users/', data)
    self.assertEqual(response.status_code, 400)

  def test_user_registration_short_password(self):
    data = {
      'email': 'a@aa.aa',
      'username': 'Athlete1234',
      'password': 'p',
      'password1': 'p',
      'phone_number': 123456789,
      'country': 'Norway',
      'city': 'Trondheim',
      'street_address': 'Gløs',
    }

    response = self.client.post('/api/users/', data)
    self.assertEqual(response.status_code, 201)
  
  def test_user_registration_short_username(self):
    data = {
      'email': 'a@aa.aa',
      'username': 'a',
      'password': '#¤252!"#AWda',
      'password1': '#¤252!"#AWda',
      'phone_number': 12354678,
      'country': 'Norway',
      'city': 'Trondheim',
      'street_address': 'Gløs',
    }

    response = self.client.post('/api/users/', data)
    self.assertEqual(response.status_code, 201)

  def test_user_registration_short_username(self):
    data = {
      'email': 'a@aa.aa',
      'username': 'a',
      'password': '#¤252!"#AWda',
      'password1': '#¤252!"#AWda',
      'phone_number': 12354678,
      'country': 'Norway',
      'city': 'Trondheim',
      'street_address': 'Gløs',
    }
    data['email'] = 'a@aa.aa'


    response = self.client.post('/api/users/', data)
    self.assertEqual(response.status_code, 201)

######### 2-way testing #################

test_cases = [
  {'email':'wrong',	'username':'wrong', 'password':'normal', 'password1':'normal', 'phone_number':'empty', 'country':'empty',	'city':'empty',	'street_address':'normal'},
  {'email':'wrong',	'username':'normal', 'password':'empty', 'password1':'normal', 'phone_number':'normal', 'country':'normal',	'city':'normal',	'street_address':'empty'},
  {'email':'wrong',	'username':'empty', 'password':'normal', 'password1':'empty', 'phone_number':'empty', 'country':'empty',	'city':'normal',	'street_address':'normal'},
  {'email':'wrong',	'username':'normal', 'password':'empty', 'password1':'empty', 'phone_number':'empty', 'country':'normal',	'city':'normal',	'street_address':'normal'},
  {'email':'wrong',	'username':'normal', 'password':'normal', 'password1':'normal', 'phone_number':'normal', 'country':'empty',	'city':'empty',	'street_address':'empty'},
  {'email':'normal',	'username':'wrong', 'password':'empty', 'password1':'normal', 'phone_number':'normal', 'country':'empty',	'city':'normal',	'street_address':'normal'},
  {'email':'normal',	'username':'normal', 'password':'normal', 'password1':'empty', 'phone_number':'empty', 'country':'normal',	'city':'normal',	'street_address':'empty'},
  {'email':'normal',	'username':'empty', 'password':'normal', 'password1':'normal', 'phone_number':'empty', 'country':'normal',	'city':'normal',	'street_address':'empty'},
  {'email':'normal',	'username':'wrong', 'password':'empty', 'password1':'empty', 'phone_number':'normal', 'country':'normal',	'city':'empty',	'street_address':'normal'},
  {'email':'empty',	'username':'empty', 'password':'empty', 'password1':'empty', 'phone_number':'normal', 'country':'normal',	'city':'empty',	'street_address':'normal'},
  {'email':'empty',	'username':'wrong', 'password':'normal', 'password1':'normal', 'phone_number':'empty', 'country':'normal',	'city':'normal',	'street_address':'empty'},
  {'email':'empty',	'username':'normal', 'password':'empty', 'password1':'normal', 'phone_number':'normal', 'country':'empty',	'city':'empty',	'street_address':'normal'},
  {'email':'empty',	'username':'normal', 'password':'normal', 'password1':'empty', 'phone_number':'empty', 'country':'normal',	'city':'normal',	'street_address':'empty'},
  {'email':'empty',	'username':'empty', 'password':'empty', 'password1':'normal', 'phone_number':'normal', 'country':'empty',	'city':'normal',	'street_address':'normal'},
  {'email':'empty',	'username':'wrong', 'password':'normal', 'password1':'empty', 'phone_number':'normal', 'country':'normal',	'city':'empty',	'street_address':'empty'},
  {'email':'empty',	'username':'normal', 'password':'normal', 'password1':'normal', 'phone_number':'empty', 'country':'empty',	'city':'normal',	'street_address':'normal'},
  {'email':'wrong',	'username':'wrong', 'password':'empty', 'password1':'empty', 'phone_number':'normal', 'country':'normal',	'city':'normal',	'street_address':'normal'},
  {'email':'wrong',	'username':'empty', 'password':'normal', 'password1':'normal', 'phone_number':'normal', 'country':'normal',	'city':'empty',	'street_address':'empty'},
  {'email':'wrong',	'username':'wrong', 'password':'normal', 'password1':'empty', 'phone_number':'empty', 'country':'empty',	'city':'normal',	'street_address':'normal'},
  {'email':'wrong',	'username':'empty', 'password':'normal', 'password1':'normal', 'phone_number':'empty', 'country':'empty',	'city':'empty',	'street_address':'normal'},
  {'email':'normal',	'username':'normal', 'password':'empty', 'password1':'normal', 'phone_number':'empty', 'country':'normal',	'city':'empty',	'street_address':'normal'},
  {'email':'normal',	'username':'normal', 'password':'normal', 'password1':'empty', 'phone_number':'normal', 'country':'empty',	'city':'normal',	'street_address':'empty'},
  {'email':'normal',	'username':'empty', 'password':'normal', 'password1':'normal', 'phone_number':'empty', 'country':'normal',	'city':'empty',	'street_address':'normal'},
  {'email':'normal',	'username':'wrong', 'password':'empty', 'password1':'normal', 'phone_number':'normal', 'country':'empty',	'city':'normal',	'street_address':'empty'},
  {'email':'normal',	'username':'normal', 'password':'normal', 'password1':'empty', 'phone_number':'normal', 'country':'normal',	'city':'empty',	'street_address':'normal'},
  {'email':'normal',	'username':'empty', 'password':'empty', 'password1':'normal', 'phone_number':'empty', 'country':'normal',	'city':'normal',	'street_address':'empty'},
  {'email':'normal',	'username':'wrong', 'password':'normal', 'password1':'empty', 'phone_number':'normal', 'country':'empty',	'city':'normal',	'street_address':'normal'}
]

case_data = {
  'normal': {
    'email': 'a@aa.aa',
    'username': 'athlete',
    'password': '#¤252!"#AWda',
    'password1': '#¤252!"#AWda',
    'phone_number': 12354678,
    'country': 'Norway',
    'city': 'Trondheim',
    'street_address': 'Gløs',
  },
  'empty': {
    'email': '',
    'username': '',
    'password': '',
    'password1': '',
    'phone_number': '',
    'country': '',
    'city': '',
    'street_address': '',
  },
  'wrong': {
    'email': 'a@aa',
    'username': '[athlete',
  }
}

class UserRegistrationTwoWayTest(TestCase):
  def setUp(self):
    self.client = Client()

  def test_user_two_way(self):
    
    for case in test_cases:
      data = {}
      # Populate the data with correct values
      for key, value in case.items():
        data[key] = case_data[value][key]
      # Find what status should be recieved
      code = 201
      if case['email'] == 'wrong' or case['username'] == 'wrong' or case['password'] == 'empty' or case['username'] == 'empty' or case['password1'] == 'empty':
        code = 400
      # Send the request
      response = self.client.post('/api/users/', data)
      # Check the response status_code
      self.assertEqual(response.status_code, code)

    