async function fetchWorkouts(ordering) {
    let response = await sendRequest("GET", `${HOST}/api/workouts/?ordering=${ordering}`);

    if (!response.ok) {
        throw new Error(`HTTP error! status: ${response.status}`);
    } else {
        let workouts = await response.json();

        let container = document.getElementById('div-content');
        workouts.forEach(workout => {
            let templateWorkout = document.querySelector("#template-workout");
            let cloneWorkout = templateWorkout.content.cloneNode(true);

            let aWorkout = cloneWorkout.querySelector("a");
            aWorkout.href = `workout.html?id=${workout.id}`;

            let h5 = aWorkout.querySelector("h5");
            h5.textContent = workout.name;

            let localDate = new Date(workout.date);

            let table = aWorkout.querySelector("table");
            let rows = table.querySelectorAll("tr");
            rows[0].querySelectorAll("td")[1].textContent = localDate.toLocaleDateString(); // Date
            rows[1].querySelectorAll("td")[1].textContent = localDate.toLocaleTimeString(); // Time
            rows[2].querySelectorAll("td")[1].textContent = workout.owner_username; //Owner
            rows[3].querySelectorAll("td")[1].textContent = workout.exercise_instances.length; // Exercises

            container.appendChild(aWorkout);
        });
        return workouts;
    }
}

function createWorkout() {
    window.location.replace("workout.html");
}

window.addEventListener("DOMContentLoaded", async () => {
    let createButton = document.querySelector("#btn-create-workout");
    createButton.addEventListener("click", createWorkout);
    let ordering = "-date";

    const urlParams = new URLSearchParams(window.location.search);
    if (urlParams.has('ordering')) {
        let aSort = null;
        ordering = urlParams.get('ordering');
        if (ordering == "name" || ordering == "owner" || ordering == "date") {
                let aSort = document.querySelector(`a[href="?ordering=${ordering}"`);
                aSort.href = `?ordering=-${ordering}`;
        } 
    } 

    let currentSort = document.querySelector("#current-sort");
    currentSort.innerHTML = (ordering.startsWith("-") ? "Descending" : "Ascending") + " " + ordering.replace("-", "");

    let currentUser = await getCurrentUser();
    // grab username
    if (ordering.includes("owner")) {
        ordering += "__username";
    }
    let workouts = await fetchWorkouts(ordering);
    
    let tabEls = document.querySelectorAll('a[data-bs-toggle="list"]');
    for (let i = 0; i < tabEls.length; i++) {
        let tabEl = tabEls[i];
        tabEl.addEventListener('show.bs.tab', function (event) {
            let workoutAnchors = document.querySelectorAll('.workout');
            let selectedWorkouts = []
            for (let j = 0; j < workouts.length; j++) {
                // I'm assuming that the order of workout objects matches
                // the other of the workout anchor elements. They should, given
                // that I just created them.
                let workout = workouts[j];
                let workoutAnchor = workoutAnchors[j];

                switch (event.currentTarget.id) {
                    case "list-my-workouts-list":
                        if (workout.owner == currentUser.url) {
                            workoutAnchor.classList.remove('hide');
                            selectedWorkouts.push(workout)
                        } else {
                            workoutAnchor.classList.add('hide');
                        }
                        break;
                    case "list-athlete-workouts-list":
                        if (currentUser.athletes && currentUser.athletes.includes(workout.owner)) {
                            workoutAnchor.classList.remove('hide');
                            selectedWorkouts.push(workout)
                        } else {
                            workoutAnchor.classList.add('hide');
                        }
                        break;
                    case "list-public-workouts-list":
                        if (workout.visibility == "PU") {
                            workoutAnchor.classList.remove('hide');
                            selectedWorkouts.push(workout)
                        } else {
                            workoutAnchor.classList.add('hide');
                        }
                        break;
                    default :
                        workoutAnchor.classList.remove('hide');
                        selectedWorkouts.push(workout)
                        break;
                }
            }
            populateWorkoutCountGraph(selectedWorkouts)
            populateExerciseTypesGraph(selectedWorkouts)
        });
    }
    populateWorkoutCountGraph(workouts)
    populateExerciseTypesGraph(workouts)
});

var workoutCountGraph = null
function populateWorkoutCountGraph(workouts) {
    let months = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Okt', 'Nov', 'Dec']
    let data = new Array(12).fill(0)
    const now = new Date()
    const yearAgo = new Date().setFullYear(now.getFullYear() - 1)

    workouts.forEach((workout) => {
        const date = new Date(workout.date)
        if (date > yearAgo) {
            data[date.getMonth()]++
        }
    })

    // Shift graph
    let i = 11 - now.getMonth()
    while(i-- > 0) {
        months.unshift(months.pop())
        data.unshift(data.pop())
    }
    
    var options = {
        series: [{
        name: "Workouts",
        data: data
    }],
        chart: {
        height: 350,
        type: 'line',
        zoom: {
        enabled: false
        }
    },
    dataLabels: {
        enabled: false
    },
    stroke: {
        curve: 'straight'
    },
    title: {
        text: 'Number of workouts completed',
        align: 'left'
    },
    grid: {
        row: {
        colors: ['#f3f3f3', 'transparent'],
        opacity: 0.5
        },
    },
    yaxis: {
        labels: {
            formatter: function(val) {
              return val.toFixed(0)
            }
        },
    },
    xaxis: {
        categories: months,
    }
    };
    if (workoutCountGraph != null) {
        workoutCountGraph.destroy()
    }
    workoutCountGraph = new ApexCharts(document.querySelector("#workoutCountGraph"), options);
    workoutCountGraph.render();
}

async function fetchExerciseTypes() {
    let response = await sendRequest("GET", `${HOST}/api/exercises/`);

    if (response.ok) {
        return await response.json();
    }
    return null
}

var exerciseTypesGraph = null
async function populateExerciseTypesGraph(workouts) {
    let exerciseTypes = await fetchExerciseTypes()

    let data = {}
    let exerciseNames = exerciseTypes.map((ex) => ex.name)
    exerciseTypes.forEach((ex) => data[ex.id] = 0)

    const now = new Date()
    const yearAgo = new Date().setFullYear(now.getFullYear() - 1)

    workouts.forEach((workout) => {
        const date = new Date(workout.date)
        if (date > yearAgo) {
            workout.exercise_instances.forEach((ex) => {
                data[ex.exercise_id]++
            })
        }
    })
    
    var options = {
        series: [{
        name: "Exercise",
        data: Object.values(data)
    }],
        chart: {
        height: 350,
        type: 'bar',
        zoom: {
        enabled: false
        }
    },
    dataLabels: {
        enabled: false
    },
    stroke: {
        curve: 'straight'
    },
    title: {
        text: 'Number of workouts containing excercises',
        align: 'left'
    },
    grid: {
        row: {
        colors: ['#f3f3f3', 'transparent'],
        opacity: 0.5
        },
    },
    theme: {
        mode: 'light', 
        palette: 'palette2', 
    },
    yaxis: {
        labels: {
            formatter: function(val) {
              return val.toFixed(0)
            }
        },
    },
    xaxis: {
        categories: exerciseNames,
    }
    };
    if (exerciseTypesGraph != null) {
        exerciseTypesGraph.destroy()
    }
    exerciseTypesGraph = new ApexCharts(document.querySelector("#exerciseTypesGraph"), options);
    exerciseTypesGraph.render();
}