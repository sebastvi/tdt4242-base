let cancelButton;
let okButton;
let deleteButton;
let editButton;
let oldFormData;


function handleCancelButtonDuringEdit() {
    setReadOnly(true, "#form-group");
    okButton.className += " hide";
    deleteButton.className += " hide";
    cancelButton.className += " hide";
    editButton.className = editButton.className.replace(" hide", "");

    cancelButton.removeEventListener("click", handleCancelButtonDuringEdit);

    let form = document.querySelector("#form-group");
    if (oldFormData.has("title")) form.title.value = oldFormData.get("title");
    if (oldFormData.has("description")) form.description.value = oldFormData.get("description");
    
    oldFormData.delete("title");
    oldFormData.delete("description");

}

function handleCancelButtonDuringCreate() {
    window.location.assign("myathletes.html");
}

async function createGroup() {
    let form = document.querySelector("#form-group");
    let formData = new FormData(form);
    let body = {"title": formData.get("title"), 
                "description": formData.get("description"), 
                };

    let response = await sendRequest("POST", `${HOST}/api/athlete-groups/`, body);

    if (response.ok) {
        window.location.assign("myathletes.html");
    } else {
        let data = await response.json();
        let alert = createAlert("Could not create new group!", data);
        document.body.prepend(alert);
    }
}

function handleEditGroupButtonClick() {
    setReadOnly(false, "#form-group");

    editButton.className += " hide";
    okButton.className = okButton.className.replace(" hide", "");
    cancelButton.className = cancelButton.className.replace(" hide", "");
    deleteButton.className = deleteButton.className.replace(" hide", "");

    cancelButton.addEventListener("click", handleCancelButtonDuringEdit);

    let form = document.querySelector("#form-group");
    oldFormData = new FormData(form);
}

async function deleteGroup(id) {
    let response = await sendRequest("DELETE", `${HOST}/api/athlete-groups/${id}/`);
    if (!response.ok) {
        let data = await response.json();
        let alert = createAlert(`Could not delete group ${id}`, data);
        document.body.prepend(alert);
    } else {
        window.location.assign("myathletes.html");
    }
}

async function retrieveGroup(id) {
    let response = await sendRequest("GET", `${HOST}/api/athlete-groups/${id}/`);
    console.log(response.ok);
    if (!response.ok) {
        let data = await response.json();
        let alert = createAlert("Could not retrieve group data!", data);
        document.body.prepend(alert);
    } else {
        let groupData = await response.json();
        let form = document.querySelector("#form-group");
        let formData = new FormData(form);

        for (let key of formData.keys()) {
            let selector = `input[name="${key}"], textarea[name="${key}"]`;
            let input = form.querySelector(selector);
            let newVal = groupData[key];
            input.value = newVal;
        }
    }
}

async function updateGroup(id) {
    let form = document.querySelector("#form-group");
    let formData = new FormData(form);
    let body = {"title": formData.get("title"), 
                "description": formData.get("description"), 
                };
    let response = await sendRequest("PUT", `${HOST}/api/athlete-groups/${id}/`, body);

    if (!response.ok) {
        let data = await response.json();
        let alert = createAlert(`Could not update group ${id}`, data);
        document.body.prepend(alert);
    } else {
        // duplicate code from handleCancelButtonDuringEdit
        // you should refactor this
        setReadOnly(true, "#form-group");
        okButton.className += " hide";
        deleteButton.className += " hide";
        cancelButton.className += " hide";
        editButton.className = editButton.className.replace(" hide", "");
    
        cancelButton.removeEventListener("click", handleCancelButtonDuringEdit);
        
        oldFormData.delete("title");
        oldFormData.delete("description");
    }

    body = {'athlete_group_id': id};
    response = await sendRequest("PATCH", `${HOST}/api/users/${4}/athlete_groups`, body);
    if (!response.ok) {
        let data = await response.json();
        let alert = createAlert("Could not update group!", data);
        document.body.prepend(alert);
    } else {
        location.reload();
        return false;
    }
}

window.addEventListener("DOMContentLoaded", async () => {
    cancelButton = document.querySelector("#btn-cancel-group");
    okButton = document.querySelector("#btn-ok-group");
    deleteButton = document.querySelector("#btn-delete-group");
    editButton = document.querySelector("#btn-edit-group");
    oldFormData = null;

    const urlParams = new URLSearchParams(window.location.search);

    // view/edit
    if (urlParams.has('id')) {
        const groupId = urlParams.get('id');
        await retrieveGroup(groupId);

        editButton.addEventListener("click", handleEditGroupButtonClick);
        deleteButton.addEventListener("click", (async (id) => await deleteGroup(id)).bind(undefined, groupId));
        okButton.addEventListener("click", (async (id) => await updateGroup(id)).bind(undefined, groupId));
    } 
    //create
    else {
        setReadOnly(false, "#form-group");

        editButton.className += " hide";
        okButton.className = okButton.className.replace(" hide", "");
        cancelButton.className = cancelButton.className.replace(" hide", "");

        okButton.addEventListener("click", async () => await createGroup());
        cancelButton.addEventListener("click", handleCancelButtonDuringCreate);
    }
});