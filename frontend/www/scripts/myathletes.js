var currentUser = null
var athletes = []
var pendingAthletes = []

async function dragEventHandler(event) {
    oldGroupId = event.from.dataset.groupId
    newGroupId = event.to.dataset.groupId
    if(oldGroupId === newGroupId) {
        return
    }
    userId = event.item.dataset.userId
    const response = await sendRequest("PATCH", `${HOST}/api/users/${userId}/athlete-groups`, { athlete_group_id: newGroupId });
    const body = await response.json()
}

function createSortable(listElement, groupId) {
    return Sortable.create(listElement, { 
        group: 'athlete-list', 
        animation: 150,
        onEnd: dragEventHandler,
        groupId,
    })
}

async function displayCurrentRoster() {
    let templateAthleteGroup = document.querySelector("#template-athlete-group");
    let templateEmptyAthlete = document.querySelector("#template-empty-athlete");

    currentUser = await getCurrentUser();
    const athleteResponses = await Promise.all(currentUser.athletes.map(athleteUrl => sendRequest("GET", athleteUrl)))
    athletes = await Promise.all(athleteResponses.map(athleteResponse => athleteResponse.json()))

    let groupResponse = await sendRequest("GET", `${HOST}/api/athlete-groups?owner=${currentUser.id}`);
    let athleteGroups = await groupResponse.json();
    athleteGroups = athleteGroups.filter(gr => gr.owner_id == currentUser.id)

    athleteGroups.forEach( group => {
        const groupElement = createGroupElement(templateAthleteGroup, group.id, group.title, group.description)
        groupElement.groupId = group.id
        const athleteList = groupElement.querySelector('.group-athlete-list')
        const sortable = createSortable(athleteList)
        athletes.forEach( athlete => {
            if (athlete.athlete_group_id == group.id) {
                createFilledRow(athlete.username, athlete.id, athleteList);
                athlete.isShown = true
            }
        })
    })

    const unassignedAthleteList = document.querySelector('#unassigned-athletes-list')
    const sortable = createSortable(unassignedAthleteList)
    athletes.filter(athlete => !athlete.isShown).forEach( athlete => {
        createFilledRow(athlete.username, athlete.id, unassignedAthleteList);
    })

    let status = "p";   // pending
    let category = "sent";  
    let offerResponse = await sendRequest("GET", `${HOST}/api/offers/?status=${status}&category=${category}`);
    if (!offerResponse.ok) {
        let data = await offerResponse.json();
        let alert = createAlert("Could not retrieve offers!", data);
        document.body.prepend(alert);
    } else {
        const pendingList = document.querySelector('#pending-athletes-list')
        const offers = await offerResponse.json();
        const pendingAthleteResponses = await Promise.all(offers.map(offer => sendRequest("GET", offer.recipient)))
        pendingAthletes =  await Promise.all(pendingAthleteResponses.map(pendingAthleteResponse => pendingAthleteResponse.json()))
        for (const pendingAthlete of pendingAthletes) {
            createFilledRow(pendingAthlete.username, pendingAthlete.id, pendingList, true);
        }
    }
    
    const newAthleteInputContainer = document.querySelector('#new-athletes-input')
    const emptyClone = templateEmptyAthlete.content.cloneNode(true);
    const emptyAthleteDiv = emptyClone.querySelector("div");
    const emptyButton = emptyAthleteDiv.querySelector("button");
    emptyButton.addEventListener("click", addAthleteOffer);
    newAthleteInputContainer.appendChild(emptyAthleteDiv);
}


function createGroupElement(templateAthleteGroup, id, title, description) {
    const groupClone = templateAthleteGroup.content.cloneNode(true);
    const groupDiv = groupClone.querySelector("div");
    const groupAthleteList = groupDiv.querySelector(".group-athlete-list")
    groupAthleteList.dataset.groupId = id
    const groupTitle = groupDiv.querySelector("h5");
    const groupDescription = groupDiv.querySelector("p");
    groupTitle.innerText = title
    groupDescription.innerText = description
    let editButton = groupDiv.querySelector("button");
    editButton.addEventListener("click", ()=>window.location.assign(`athletegroup.html?id=${id}`));
    document.querySelector('#athlete-group-list').appendChild(groupDiv);

    return groupDiv
}

function createFilledRow(inputValue, userId, rowContainer, pending = false) {
    const selector = pending ? "#template-filled-pending" : "#template-filled-athlete"
    const filledClone = document.querySelector(selector).content.cloneNode(true);
    const filledDiv = filledClone.querySelector("div");
    filledDiv.dataset.userId = userId
    const filledInput = filledDiv.querySelector("input");
    filledInput.value = pending ? `${inputValue} (pending)` : inputValue;
    filledInput.disabled = true;
    if(!pending) {
        const filledButton = filledDiv.querySelector("button");
        filledButton.addEventListener("click", (event) => removeAthleteRow(event, userId));
    }
    rowContainer.appendChild(filledDiv);
}

async function removeAthleteRow(event, userId) {
    const constainer = event.target.closest('.input-group')
    body = { athletes: athletes.filter(ath => ath.id !== userId).map(ath => ath.id) }
    const response = await sendRequest("PUT", currentUser.url + 'athletes', body);
    if(response.ok) {
        constainer.remove()
        athletes = athletes.filter(ath => ath.id !== userId)
    }
}

async function addAthleteOffer() {
    const username = document.querySelector('#new-athletes-input input').value
    if(!username) {
        return
    }
    document.querySelector('#new-athletes-input input').value = ''
    if(athletes.find(ath => ath.username === username)) {
        // if the user is already in our list
        showAlert("You are already coaching this athlete.")
        return
    }
    if(pendingAthletes.find(ath => ath.username === username)) {
        // if the user is already in our list
        showAlert("You already have an offer out for this athlete.")
        return
    }
    const response = await sendRequest("GET", `${HOST}/api/users/${username}/`);
    const athlete = await response.json()
    
    // create offer
    const body = {'status': 'p', 'recipient': athlete.url};
    const offerResponse = await sendRequest("POST", `${HOST}/api/offers/`, body);
    if (!offerResponse.ok) {
        showAlert("Could not create offer!", await response.json())
        return
    }

    createFilledRow(username, athlete.id, document.querySelector('#pending-athletes-list'), true)
    athletes.push(athlete)
}

async function displayFiles() {
    let templateAthlete = document.querySelector("#template-athlete-tab");
    let templateFiles = document.querySelector("#template-files");
    let templateFile = document.querySelector("#template-file");
    let listTab = document.querySelector("#list-tab");
    let navTabContent = document.querySelector("#nav-tabContent");

    for (let fileUrl of currentUser.athlete_files) {
        let response = await sendRequest("GET", fileUrl);
        let file = await response.json();

        response = await sendRequest("GET", file.athlete);
        let athlete = await response.json();

        let tabPanel = document.querySelector(`#tab-contents-${athlete.username}`)
        if (!tabPanel) {
            tabPanel = createTabContents(templateAthlete, athlete, listTab, templateFiles, navTabContent);
        } 

        let divFiles = tabPanel.querySelector(".uploaded-files");
        let aFile = createFileLink(templateFile, file.file);

        divFiles.appendChild(aFile);
    }

    for (let athlete of athletes) {
        let tabPanel = document.querySelector(`#tab-contents-${athlete.username}`)
        if (!tabPanel) {
            tabPanel = createTabContents(templateAthlete, athlete, listTab, templateFiles, navTabContent);
        }
        let uploadBtn = document.querySelector(`#btn-upload-${athlete.username}`);
        uploadBtn.disabled = false;
        uploadBtn.addEventListener("click", async (event) => await uploadFiles(event, athlete));

        let fileInput = tabPanel.querySelector(".form-control");
        fileInput.disabled = false;
    }

    if (currentUser.athlete_files.length == 0 && currentUser.athletes.length == 0) {
        let p = document.createElement("p");
        p.innerText = "There are currently no athletes or uploaded files.";
        document.querySelector("#list-files-div").append(p);
    }
}

function createTabContents(templateAthlete, athlete, listTab, templateFiles, navTabContent) {
    let cloneAthlete = templateAthlete.content.cloneNode(true);

    let a = cloneAthlete.querySelector("a");
    a.id = `tab-${athlete.username}`;
    a.href = `#tab-contents-${athlete.username}`;
    a.text = athlete.username;
    listTab.appendChild(a);

    let tabPanel = templateFiles.content.firstElementChild.cloneNode(true);
    tabPanel.id = `tab-contents-${athlete.username}`;

    let uploadBtn = tabPanel.querySelector('input[value="Upload"]');
    uploadBtn.id = `btn-upload-${athlete.username}`;

    navTabContent.appendChild(tabPanel);
    return tabPanel;
}

function createFileLink(templateFile, fileUrl) {
    let cloneFile = templateFile.content.cloneNode(true);
    let aFile = cloneFile.querySelector("a");
    aFile.href = fileUrl;
    let pathArray = fileUrl.split("/");
    aFile.text = pathArray[pathArray.length - 1];
    return aFile;
}


async function uploadFiles(event, athlete) {
    let form = event.currentTarget.parentElement;
    let inputFormData = new FormData(form);
    let templateFile = document.querySelector("#template-file");

    for (let file of inputFormData.getAll("files")) {
        if (file.size > 0) {
            let submitForm = new FormData();
            submitForm.append("file", file);
            submitForm.append("athlete", athlete.url);

            let response = await sendRequest("POST", `${HOST}/api/athlete-files/`, submitForm, "");
            if (response.ok) {
                let data = await response.json();

                let tabPanel = document.querySelector(`#tab-contents-${athlete.username}`)
                let divFiles = tabPanel.querySelector(".uploaded-files");
                let aFile = createFileLink(templateFile, data["file"]);
                divFiles.appendChild(aFile);
            } else {
                let data = await response.json();
                let alert = createAlert("Could not upload files!", data);
                document.body.prepend(alert);
            }
        }
    }
}

window.addEventListener("DOMContentLoaded", async () => {
    await displayCurrentRoster();
    await displayFiles();
});

const createButton = document.querySelector("#btn-create-athlete-group");
createButton.addEventListener("click", () => window.location.assign("athletegroup.html"));