var currentUser = null;

const hide = selector => document.querySelector(selector).classList.add('d-none')
const show = selector => document.querySelector(selector).classList.remove('d-none')

async function populateProfileForm(userInput = null, readable = null) {
    disableButtons(true)
    let user = userInput || currentUser
    if(user === null) {
        user = currentUser = await getCurrentUser()
    }
    const form = document.querySelector("#profile-form");
    setReadOnly(false, "#profile-form")
    const formData = new FormData(form);
    for (let key of formData.keys()) {
        const formElement = document.querySelector(`input[name="${key}"], select[name="${key}"]`)
        formElement.value = typeof user[key] === 'string' ? user[key] : '-'
    }
    setReadOnly(true, "#profile-form")

    if(user.id !== currentUser.id) {
        document.querySelector("#show-own-profile-button").classList.remove('d-none')
    } else {
        document.querySelector("#show-own-profile-button").classList.add('d-none')
        toggleButtonVisibility(true)
    }
    disableButtons(false)
}

function disableButtons(disabled = true) {
    document.querySelectorAll(".container button").forEach(button => {
        disabled 
            ? button.setAttribute('disabled', 'disabled') 
            : button.removeAttribute('disabled')
    })
}

function toggleButtonVisibility(value = null) {
    if(value === null) {
        value = document.querySelector("#profile-edit-button").classList.contains('d-none')
    }
    
    console.log(currentUser)

    if (value) {
        if(currentUser.coach !== null) {
            show("#show-coach-button")
        }
        show("#profile-edit-button")
        hide("#profile-save-button")
        hide("#profile-cancel-button")
    } else {
        hide("#show-coach-button")
        hide("#profile-edit-button")
        show("#profile-save-button")
        show("#profile-cancel-button")
    }
}

function hideAllUserFormButtons() {
    hide("#show-coach-button")
    hide("#profile-edit-button")
    hide("#profile-save-button")
    hide("#profile-cancel-button")
}

function editProfile() {
    setReadOnly(false, "#profile-form")
    toggleButtonVisibility(false)
}

async function saveProfile() {
    const formData = new FormData(document.querySelector("#profile-form"))
    const body = {}
    formData.forEach((value, key) => body[key] = value)
    const response = await sendRequest("PUT", currentUser.url, body);
    if(!response.ok) {
        showAlert("Could not update user.", data)
        return
    } else {
        currentUser = {...currentUser, ...body}
    }
    setReadOnly(true, "#profile-form")
    toggleButtonVisibility(true)
}

function cancelEditProfile() {
    populateProfileForm()
    toggleButtonVisibility(true)
}

async function searchForUser(event, searchUrl = null) {
    hideAllUserFormButtons()
    if(searchUrl === null) {
        const input = document.querySelector("#user-search").value
        if(!input) {
            await populateProfileForm()
            return
        }
        searchUrl =  `${HOST}/api/users/${input}/`
    }
    
    const response = await sendRequest("GET", searchUrl)
    if(response.ok) {
        const user = await response.json()
        await populateProfileForm(user, false)
    } else {
        const data = await response.json()
        showAlert("User not found.", data)
    }
}

function showOwnProfile() {
    document.querySelector("#user-search").value = ''
    populateProfileForm()
}

function showCoachProfile(event) {
    searchForUser(event, currentUser.coach)
}

window.addEventListener("DOMContentLoaded", async () => {
    await populateProfileForm()
    document.querySelector("#profile-edit-button").onclick = editProfile
    document.querySelector("#profile-save-button").onclick = saveProfile
    document.querySelector("#profile-cancel-button").onclick = cancelEditProfile
    document.querySelector("#user-search-button").onclick = searchForUser
    document.querySelector("#show-own-profile-button").onclick = showOwnProfile
    document.querySelector("#show-coach-button").onclick = showCoachProfile
})

