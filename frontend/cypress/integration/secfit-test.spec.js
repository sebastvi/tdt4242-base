/// <reference types="Cypress" />

describe('Workout page', () => {
  it('Should show graphs', function() {
    this.login(this.athleteUser)

    cy.get('#workoutCountGraph', { timeout: 10000 }).should('be.visible')
    cy.get('#exerciseTypesGraph', { timeout: 10000 }).should('be.visible')
    
    cy.get('#list-my-workouts-list').click()
    cy.get('#workoutCountGraph', { timeout: 10000 }).should('be.visible')
    cy.get('#exerciseTypesGraph', { timeout: 10000 }).should('be.visible')

    cy.get('#list-athlete-workouts-list').click()
    cy.get('#workoutCountGraph', { timeout: 10000 }).should('be.visible')
    cy.get('#exerciseTypesGraph', { timeout: 10000 }).should('be.visible')

    cy.get('#list-public-workouts-list').click()
    cy.get('#workoutCountGraph', { timeout: 10000 }).should('be.visible')
    cy.get('#exerciseTypesGraph', { timeout: 10000 }).should('be.visible')
  })
})


describe('Athletes/coach page', () => {
  it('Should allow athlete-adding', function() {
    this.login(this.coachUser)
    cy.get('#nav-myathletes').click()
    cy.url().should('include', 'myathletes.html')
    cy.intercept('http://localhost:8000/api/athlete-groups').as('addAthleteRequest')
    cy.get('input[name="athlete"]:not([disabled])', { timeout: 10000 })
      .focus()
      .type(this.athleteUser.username)
      .parent()
      .children('button')
      .first()
      .click()
    
    cy.get('#pending-athletes-list')
      .children()
      .last()
      .children('input')
      .first()
      .should('contain.value', this.athleteUser.username)
      .should('contain.value', '(pending)')
  })

  it('Should allow coach accepting', function() {
    this.login(this.athleteUser)
    cy.get('#nav-mycoach').click()
    cy.url().should('include', 'mycoach.html')
    cy.intercept({method: 'PATCH'}).as('acceptOfferRequest')
    cy.get('#list-offers', { timeout: 10000 })
      .children()
      .first()
      .should('contain.text', this.coachUser.username)
      .should('contain.text', 'wants to be your coach')
      .children('.btn-group')
      .first()
      .children('.btn.btn-success')
      .first()
      .should('contain.text', 'Accept')
      .click()
    
    cy.wait('@acceptOfferRequest')
    cy.wait(500)

    cy.get('#input-coach').should('contain.value', this.coachUser.username)
  })

  it('Can add group', function() {
    this.login(this.coachUser)
    cy.get('#nav-myathletes').click()
    cy.url().should('include', 'myathletes.html')
    
    cy.get('#btn-create-athlete-group').click()
    cy.url().should('include', 'athletegroup.html')
    
    const title = 'Testinggroup'
    const description = 'Group for testing-purposes'
    
    cy.get('#inputTitle').type(title)
    cy.get('#inputDescription').type(description)
    cy.get('#btn-ok-group').click()
    cy.url().should('include', 'myathletes.html')

    cy.get('#athlete-group-list')
      .children('.group-element', { timeout: 10000 })
      .should('have.length', 1)
      .first()
      .should('contain.text', title)
      .should('contain.text', description)

    cy.get('#unassigned-athletes-list')
      .children('.entry')
      .first()
      // Dragging: 
      .drag('#athlete-group-list .group-athlete-list')

    cy.get('#athlete-group-list .group-athlete-list .entry')
      .should('exist')
  })
})

describe('Profile page', () => {
  it('Coach can see their profile, and edit it', function() {
    this.login(this.coachUser)
    cy.get('#btn-profile').click()
    cy.url().should('include', 'profile.html')

    cy.get('input[name="username"]').should('contain.value', this.coachUser.username)

    cy.get('#profile-edit-button').click()
    
    cy.get('select[name="visibility"]')
      .should('have.value', 'PU')
      .should('not.be.disabled')
      .select('PR')
      .should('have.value', 'PR')

    cy.get('input[name="email"]')
      .should('have.value', '')
      .type(this.coachUser.email)
      .should('have.value', this.coachUser.email)

    cy.get('input[name="phone_number"]')
      .should('have.value', '')
      .type(this.coachUser.phone_number)
      .should('have.value', this.coachUser.phone_number)

    cy.get('input[name="street_address"]')
      .should('have.value', '')
      .type(this.coachUser.street_address)
      .should('have.value', this.coachUser.street_address)

    cy.get('input[name="city"]')
      .should('have.value', '')
      .type(this.coachUser.city)
      .should('have.value', this.coachUser.city)

    cy.get('input[name="country"]')
      .should('have.value', '')
      .type(this.coachUser.country)
      .should('have.value', this.coachUser.country)
    
    cy.intercept({method: 'PUT', pathname: /\/api\/users\//})
      .as('updateCoachUser')
    cy.get('#profile-save-button')
      .click()
    cy.wait('@updateCoachUser')
  })

  it('Coach can see Athlete\'s profile, but not edit it', function() {
    this.login(this.coachUser)
    cy.get('#btn-profile').click()
    cy.url().should('include', 'profile.html')

    cy.get('input[name="user-search"]').type(this.athleteUser.username)
    cy.intercept(`http://localhost:8000/api/users/${this.athleteUser.username}/`).as('fetchAthleteUser')
    cy.get('#user-search-button')
      .should('not.be.disabled')
      .click()
    cy.wait('@fetchAthleteUser')

    cy.get('input[name="username"]')
      .should('contain.value', this.athleteUser.username)
      .should('have.attr', 'readonly')

    cy.get('#profile-edit-button')
      .should('not.be.visible')
    
    cy.get('select[name="visibility"]')
      .should('have.value', 'PU')
  })

  it('Athlete can\t see Coach\'s personal profile info, and can set their own visibility to Coach only', function() {
    this.login(this.athleteUser)
    cy.get('#btn-profile')
      .click()
    cy.url()
      .should('include', 'profile.html')
    
    cy.get('input[name="username"]')
      .should('contain.value', this.athleteUser.username)

    cy.intercept(`http://localhost:8000/api/users/`).as('fetchCoachUser')
    cy.get('#show-coach-button')
      .click()
    cy.wait('@fetchCoachUser')

    cy.get('input[name="username"]')
      .should('contain.value', this.coachUser.username)
      .should('have.attr', 'readonly')

    cy.get('#profile-edit-button')
      .should('not.be.visible')
    
    cy.get('select[name="visibility"]')
      .should('have.value', 'PR')
    
    cy.get('input[name="email"]')
      .should('have.value', '-') 

    cy.get('#show-own-profile-button')
      .click()
    
    cy.get('input[name="username"]')
      .should('contain.value', this.athleteUser.username)

      
    cy.get('#profile-edit-button')
      .click()

    cy.get('input[name="email"]')
      .should('have.value', '')
      .type(this.athleteUser.email)
      .should('have.value', this.athleteUser.email)

    cy.get('input[name="phone_number"]')
      .should('have.value', '')
      .type(this.athleteUser.phone_number)
      .should('have.value', this.athleteUser.phone_number)

    cy.get('input[name="street_address"]')
      .should('have.value', '')
      .type(this.athleteUser.street_address)
      .should('have.value', this.athleteUser.street_address)

    cy.get('input[name="city"]')
      .should('have.value', '')
      .type(this.athleteUser.city)
      .should('have.value', this.athleteUser.city)

    cy.get('input[name="country"]')
      .should('have.value', '')
      .type(this.athleteUser.country)
      .should('have.value', this.athleteUser.country)

    cy.get('select[name="visibility"]')
      .should('have.value', 'PU')
      .select('CO')
      .should('have.value', 'CO')
    
    
    cy.intercept({method: 'PUT', pathname: /\/api\/users\//})
      .as('updateAthleteUser')
    cy.get('#profile-save-button')
      .click()
    cy.wait('@updateAthleteUser')

  })
  it('Coach can see athlete\'s full profile', function() {
    this.login(this.coachUser)
    cy.get('#btn-profile')
      .click()
    cy.url()
      .should('include', 'profile.html')
    
    cy.get('input[name="user-search"]')
      .type(this.athleteUser.username)
    cy.intercept(`http://localhost:8000/api/users/${this.athleteUser.username}/`).as('fetchAthleteUser')
    cy.get('#user-search-button')
      .should('not.be.disabled')
      .click()
    cy.wait('@fetchAthleteUser')

    cy.get('input[name="username"]')
      .should('have.value', this.athleteUser.username)
      .should('have.attr', 'readonly')

    cy.get('select[name="visibility"]')
      .should('have.value', 'CO')

    cy.get('input[name="email"]')
      .should('not.have.value', '-')
      .should('have.value', this.athleteUser.email)

  })

})


