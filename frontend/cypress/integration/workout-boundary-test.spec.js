/// <reference types="Cypress" />
describe('Create workout', () => {
  it('Workout normal', function() {
    this.login(this.athleteUser)
    cy.get('#btn-create-workout').click()
    cy.url().should('include', 'workout.html')

    typeIn(
      'Workout',
      '2021-03-12T08:30',
      'Notes',
      'Private',
      'Crunch',
      '5',
      '12',
    )
    cy.intercept({method: 'POST', url: '/api/workouts/' }).as('createRequest')
    cy.get('#btn-ok-workout').click()
    cy.wait('@createRequest')
    // Should successfully create workout
    cy.url().should('include', 'workouts.html')
  })

  it('Workout empty name', function() {
    this.login(this.athleteUser)
    cy.get('#btn-create-workout').click()
    cy.url().should('include', 'workout.html')
    typeIn(
      '{end}',
      '2021-03-12T08:30',
      'Notes',
      'Private',
      'Crunch',
      '5',
      '12',
    )
    cy.intercept({method: 'POST', url: '/api/workouts/' }).as('createRequest')
    cy.get('#btn-ok-workout').click()
    cy.wait('@createRequest')
    // Should not create workout
    cy.url().should('include', 'workout.html')
  })

  it('Workout long text ok', function() {
    this.login(this.athleteUser)
    cy.get('#btn-create-workout').click()
    cy.url().should('include', 'workout.html')
    typeIn(
      'LongTextLongTextLongTextLongTextLongTextLongTextLongTextLongTextLongTextLongTextLongTextLong',
      '2021-03-12T08:30',
      'LongTextLongTextLongTextLongTextLongTextLongTextLongTextLongTextLongTextLongTextLongTextLongTextLongTextLongTextLongTextLongTextLongTextLongTextLongTextLongTextLongTextLongText',
      'Coach',
      'Push-up',
      '5',
      '12',
    )
    cy.intercept({method: 'POST', url: '/api/workouts/' }).as('createRequest')
    cy.get('#btn-ok-workout').click()
    cy.wait('@createRequest')
    // Should successfully create workout
    cy.url().should('include', 'workouts.html')
  })

  it('Workout long text fail', function() {
    this.login(this.athleteUser)
    cy.get('#btn-create-workout').click()
    cy.url().should('include', 'workout.html')
    typeIn(
      'LongTextLongTextLongTextLongTextLongTextLongTextLongTextLongTextLongTextLongTextLongTextLongTextLongTextLong',
      '2021-03-12T08:30',
      'LongTextLongTextLongTextLongTextLongTextLongTextLongTextLongTextLongTextLongTextLongTextLongTextLongTextLongTextLongTextLongTextLongTextLongTextLongTextLongTextLongTextLongText',
      'Coach',
      'Push-up',
      '5',
      '12',
    )
    cy.intercept({method: 'POST', url: '/api/workouts/' }).as('createRequest')
    cy.get('#btn-ok-workout').click()
    cy.wait('@createRequest')
    // Should not create workout
    cy.url().should('include', 'workout.html')
  })
  
  it('Workout empty date', function() {
    this.login(this.athleteUser)
    cy.get('#btn-create-workout').click()
    cy.url().should('include', 'workout.html')
    typeIn(
      'Workout',
      '0000-00-00T00:00',
      'Notes',
      'Coach',
      'Push-up',
      '5',
      '12',
    )
    cy.get('#btn-ok-workout').click()
    // Should not create workout
    cy.url().should('include', 'workout.html')
  })

  it('Workout incorrect date', function() {
    this.login(this.athleteUser)
    cy.get('#btn-create-workout').click()
    cy.url().should('include', 'workout.html')
    typeIn(
      'Workout',
      '9999-99-99T99:99',
      'Notes',
      'Coach',
      'Push-up',
      '5',
      '12',
    )
    cy.get('#btn-ok-workout').click()
    // Should not create workout
    cy.url().should('include', 'workout.html')
  })

  it('Workout incorrect time date', function() {
    this.login(this.athleteUser)
    cy.get('#btn-create-workout').click()
    cy.url().should('include', 'workout.html')
    typeIn(
      'Workout',
      '2021-03-10T30:25',
      'Notes',
      'Coach',
      'Push-up',
      '5',
      '12',
    )
    cy.get('#btn-ok-workout').click()
    // Should not create workout
    cy.url().should('include', 'workout.html')
  })

  it('Workout empty notes', function() {
    this.login(this.athleteUser)
    cy.get('#btn-create-workout').click()
    cy.url().should('include', 'workout.html')
    typeIn(
      'Workout',
      '2021-03-12T08:30',
      '{end}',
      'Coach',
      'Push-up',
      '5',
      '12',
    )
    cy.intercept({method: 'POST', url: '/api/workouts/' }).as('createRequest')
    cy.get('#btn-ok-workout').click()
    cy.wait('@createRequest')
    // Should not create workout
    cy.url().should('include', 'workout.html')
  })
  it('Workout sets and number zero', function() {
    this.login(this.athleteUser)
    cy.get('#btn-create-workout').click()
    cy.url().should('include', 'workout.html')
    typeIn(
      'Workout',
      '2021-03-12T08:30',
      'Notes',
      'Coach',
      'Push-up',
      '0',
      '0',
    )
    cy.intercept({method: 'POST', url: '/api/workouts/' }).as('createRequest')
    cy.get('#btn-ok-workout').click()
    cy.wait('@createRequest')
    // Should probably not successfully create workout, but does until further changes 
    cy.url().should('include', 'workouts.html')
  })

  it('Workout sets negative', function() {
    this.login(this.athleteUser)
    cy.get('#btn-create-workout').click()
    cy.url().should('include', 'workout.html')
    typeIn(
      'Workout',
      '2021-03-12T08:30',
      'Notes',
      'Coach',
      'Push-up',
      '-10',
      '5',
    )
    cy.intercept({method: 'POST', url: '/api/workouts/' }).as('createRequest')
    cy.get('#btn-ok-workout').click()
    cy.wait('@createRequest')
    // Should probably not successfully create workout, but does until further changes 
    cy.url().should('include', 'workouts.html')
  })

  it('Workout number negative', function() {
    this.login(this.athleteUser)
    cy.get('#btn-create-workout').click()
    cy.url().should('include', 'workout.html')
    typeIn(
      'Workout',
      '2021-03-12T08:30',
      'Notes',
      'Coach',
      'Push-up',
      '10',
      '-5',
    )
    cy.intercept({method: 'POST', url: '/api/workouts/' }).as('createRequest')
    cy.get('#btn-ok-workout').click()
    cy.wait('@createRequest')
    // Should probably not successfully create workout, but does until further changes 
    cy.url().should('include', 'workouts.html')
  })

  it('Workout more exercises', function() {
    this.login(this.athleteUser)
    cy.get('#btn-create-workout').click()
    cy.url().should('include', 'workout.html')
    cy.get('#btn-add-exercise').click({force: true})
    typeIn(
      'Workout',
      '2021-03-12T08:30',
      'Notes',
      'Public',
      'Push-up',
      '10',
      '10',
    )
    cy.get('select[name="type"]').eq(1).select('Plank', {force: true})
    cy.get('input[name="sets"]').eq(1).type(5, {force: true})
    cy.get('input[name="number"]').eq(1).type(10, {force: true})

    cy.intercept({method: 'POST', url: '/api/workouts/' }).as('createRequest')
    cy.get('#btn-ok-workout').click()
    cy.wait('@createRequest')
    // Should successfully create workout
    cy.url().should('include', 'workouts.html')
  })

  function typeIn(name, dateTime, notes, visibility, type, sets, number) {
    cy.get('#inputName').type(name)
    cy.get('#inputDateTime').type(dateTime)
    cy.get('#inputNotes').type(notes)
    cy.get('#inputVisibility').select(visibility)
    // cy.get('body').scrollTo('bottom')
    cy.get('select[name="type"]').eq(0).select(type, {force: true})
    cy.get('input[name="sets"]').eq(0).type(sets, {force: true})
    cy.get('input[name="number"]').eq(0).type(number, {force: true})
    // cy.get('#input[name=files]').type(description)
  }

})