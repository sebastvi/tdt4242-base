// ***********************************************************
// This example support/index.js is processed and
// loaded automatically before your test files.
//
// This is a great place to put global configuration and
// behavior that modifies Cypress.
//
// You can change the location of this file or turn off
// automatically serving support files with the
// 'supportFile' configuration option.
//
// You can read more here:
// https://on.cypress.io/configuration
// ***********************************************************

// Import commands.js using ES2015 syntax:
import './commands'

var coachUser = {
  username: null,
  password: null,
  email: 'coach@example.com',
  phone_number: '12345678',
  street_address: 'Roadstreet 1',
  city: 'Citytown',
  country: 'Countryland',
}
var athleteUser = {
  username: null,
  password: null,
  email: 'athlete@example.com',
  phone_number: '98765432',
  street_address: 'Streetroad 2',
  city: 'Towncity',
  country: 'Landcountry',
}
var apiUrl = "localhost:8000"
var baseUrl = "localhost:5000"

// generateId :: Integer -> String
function getRandomString(len=40) {
  const dec2hex = dec => dec.toString(16).padStart(2, "0")

  var arr = new Uint8Array(len / 2)
  window.crypto.getRandomValues(arr)
  return Array.from(arr, dec2hex).join('')
}

function generateUser() {
  return {
    username: `test_user_${getRandomString(10)}`,
    password: getRandomString(10),
  }
}

function registerUser(user) {
  cy.visit(`${baseUrl}/index.html`)
  cy.contains('Welcome to SecFit!')
  cy.get('#btn-register').click()

  cy.url().should('include', 'register.html')

  cy.get('input[name="username"]').type(user.username)
  
  cy.get('input[name="password"]').type(user.password)
  cy.get('input[name="password1"]').type(user.password)
  
  cy.get('#btn-create-account').click()
  cy.url().should('include', 'workouts.html')
  // cy.log("Waiting 1.5 seconds to allow request to go through. Should probably be replaced with a register-watcher, but hey watcha gonna doooo")
  // cy.wait(1500)
  logout()

}

function login(user) {
  cy.visit(`${baseUrl}/index.html`)
  cy.contains('Welcome to SecFit!')
  cy.get('#btn-login-nav').click()

  cy.url().should('include', 'login.html')

  cy.get('input[name="username"]').type(user.username)
  
  cy.get('input[name="password"]').type(user.password)
  
  cy.intercept({method: 'GET', url: '/workouts.html' }).as('loginRequest')
  cy.get('#btn-login').click()
  cy.wait('@loginRequest')
}

function logout() {
  cy.get('#btn-logout', { timeout: 10000 }).click()
  cy.url().should('include', 'index.html')
}


before(() => {
  coachUser = {
    ...coachUser, 
    ...generateUser()
  }
  athleteUser = {
    ...athleteUser, 
    ...generateUser()
  }
  registerUser(coachUser)
  registerUser(athleteUser)
  cy.visit(`${baseUrl}/index.html`)
})

beforeEach(() => {
  cy.wrap(coachUser).as('coachUser')
  cy.wrap(athleteUser).as('athleteUser')
  cy.wrap(login).as('login')
})

